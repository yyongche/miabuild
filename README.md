# README

## Prerequisites

- Git & Git LFS
- Docker & Docker Compose
- PostgreSQL Client 15
- Python 3.8 & Pipenv
- NodeJS 18
- Visual Studio Code

## Get Packages For Offline Development

1. Run vscode task `dev-create-offline`.
2. Run vscode task `dev-create-offline-venv`.
3. Get all the files in the directory `offline`.

## Add/Remove Dependencies In `algo-gpu`

1. Modify the file `_requirements.txt` to add Python packages.
2. Modify the file `Dockerfile` to add Ubuntu packages if needed.
3. Run vscode task `algo-gpu-init`.
4. (Optional) Modify the version `ALGO_GPU_BASE_VERSION=x.y.z` in the file `../.env`.
5. Run vscode task `algo-gpu-create-offline`.

## Add/Remove Dependencies In `ui`

1. Modify the dependencies via the command `npm`.
2. Run vscode task `ui-init`.
3. (Optional) Modify the version `UI_BASE_VERSION=x.y.z` in the file `../.env`.
4. Run vscode task `ui-create-offline`.

## Development

### Update Python Package `tasklib`

1. Update the version in the file `./setup.py`.
2. Run vscode task `tasklib-package-wheel`.

### Update Python Program `uploader`

1. Run vscode task `uploader-package-program`.

### Run Unit Testing In `tasklib`

1. Run vscode task `dev-api-run`.
2. Run the command `pytest -s`.
3. Run vscode task `dev-api-stop`.

### Run Unit Testing In `api`

1. Run vscode task `dev-api-run`.
2. Run the command `./run_worker.sh`.
3. Run the command `pytest -s`.
4. Run vscode task `dev-api-stop`.

### Run Unit Testing In `backend`

1. Run vscode task `dev-backend-run`.
2. Run the command `./run_worker.sh` under the directory `api`.
3. Run the command `npm run test:e2e`.
4. Run vscode task `dev-backend-stop`.
