#!/usr/bin/env bash

MIA_ASSET_FOLDER=tests/ MIA_WIP_FOLDER=tests/ python -m celery \
    -A tests.worker worker -l DEBUG -Q dev,cpu,gpu,batch,celery
rm -rf tests/tmp*
