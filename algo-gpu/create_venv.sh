#!/usr/bin/env bash

function add_pytorch () {

sed -i '/^\[packages\]/d' Pipfile
cat <<EOF >> Pipfile

[[source]]
url = "https://download.pytorch.org/whl/cu116/"
verify_ssl = false
name = "pytorch"

[packages]
torch = {version = "==1.13.1", index = "pytorch"}
torchaudio = {version = "==0.13.1", index = "pytorch"}
torchvision = {version = "==0.14.1", index = "pytorch"}
EOF

}

if [ -d .venv ]; then
    pipenv --rm
    rm -rf .venv/
fi
export PIPENV_VENV_IN_PROJECT=1
if [ -f Pipfile.lock ]; then
    pipenv --verbose install --dev --python 3.8
    pipenv run pip install --trusted-host pypi.ngc.nvidia.com \
        --extra-index-url https://pypi.ngc.nvidia.com \
        --no-deps -r requirements-tensorflow.txt
    pipenv --verbose install -r _requirements.txt
else
    rm Pipfile
    pipenv --python 3.8
    add_pytorch
    pipenv --verbose install --dev -r requirements-dev.txt
    pipenv --verbose install -r requirements-tasklib.txt
    pipenv run pip install --trusted-host pypi.ngc.nvidia.com \
        --extra-index-url https://pypi.ngc.nvidia.com \
        --no-deps -r requirements-tensorflow.txt
    pipenv --verbose install -r _requirements.txt

    whlfile=`ls -1 mia_tasklib-*-py3-none-any.whl`
    pipenv uninstall mia_tasklib
    pipenv install ${whlfile}
fi
