-i https://pypi.org/simple
anyio==3.6.2 ; python_full_version >= '3.6.2'
certifi==2022.12.7 ; python_version >= '3.6'
charset-normalizer==3.1.0 ; python_full_version >= '3.7.0'
flet==0.4.2
flet-core==0.4.2 ; python_version >= '3.7' and python_version < '4.0'
h11==0.14.0 ; python_version >= '3.7'
httpcore==0.16.3 ; python_version >= '3.7'
httpx==0.23.3 ; python_version >= '3.7'
idna==3.4 ; python_version >= '3.5'
oauthlib==3.2.2 ; python_version >= '3.6'
packaging==23.0 ; python_version >= '3.7'
repath==0.9.0
requests==2.28.2
requests-toolbelt==0.10.1
rfc3986[idna2008]==1.5.0
six==1.16.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
sniffio==1.3.0 ; python_version >= '3.7'
urllib3==1.26.15 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'
watchdog==2.3.1 ; python_version >= '3.6'
watchfiles==0.18.1
websocket-client==1.5.1 ; python_version >= '3.7'
websockets==10.4 ; python_version >= '3.7'
