#!/usr/bin/env bash

rm -rf build/ dist/ uploader.spec
pipenv requirements > requirements.txt
docker run -it --rm -v $(pwd):/src -e PLATFORMS=linux \
    -e SHELL_CMDS="pip install --upgrade setuptools pip pyinstaller" \
    fydeinc/pyinstaller uploader.py
rm -rf uploader.spec
