#!/usr/bin/env bash

source .env
cp .env offline/versions

VALID_ARGS=$(getopt -o bakglud --long base,api,backend,gpu,uploader,ui,dev -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
    case "$1" in
        -b | --base)
            if [ -f offline/redis.tar.gz ]; then
                rm offline/redis.tar.gz
            fi
            docker pull redis:${REDIS_VERSION}
            docker save redis:${REDIS_VERSION} | \
                gzip > offline/redis.tar.gz
            if [ -f offline/postgresql.tar.gz ]; then
                rm offline/postgresql.tar.gz
            fi
            docker pull postgres:${POSTGRESQL_VERSION}
            docker save postgres:${POSTGRESQL_VERSION} | \
                gzip > offline/postgresql.tar.gz
            if [ -f offline/minio.tar.gz ]; then
                rm offline/minio.tar.gz
            fi
            docker pull minio/minio:${MINIO_VERSION}
            docker save minio/minio:${MINIO_VERSION} | \
                gzip > offline/minio.tar.gz
            if [ -f offline/nginx.tar.gz ]; then
                rm offline/nginx.tar.gz
            fi
            docker pull nginx:${NGINX_VERSION}
            docker save nginx:${NGINX_VERSION} | \
                gzip > offline/nginx.tar.gz

            shift
            ;;
        -a | --api)
            if [ -f offline/mia-api.tar.gz ]; then
                rm offline/mia-api.tar.gz
            fi
            docker save mia-api:${API_VERSION} | \
                gzip > offline/mia-api.tar.gz

            shift
            ;;
        -k | --backend)
            if [ -f offline/mia-backend.tar.gz ]; then
                rm offline/mia-backend.tar.gz
            fi
            docker save mia-backend:${BACKEND_VERSION} | \
                gzip > offline/mia-backend.tar.gz

            if [ -f /offline/mia-schema.sql ]; then
                rm offline/mia-schema.sql
            fi
            cp mia-schema.sql offline/mia-schema.sql

            shift
            ;;
        -g | --gpu)
            if [ -f offline/mia-algo-gpu-base.tar.gz ]; then
                rm offline/mia-algo-gpu-base.tar.gz
            fi
            docker save mia-algo-gpu-base:${ALGO_GPU_BASE_VERSION} | \
                gzip > offline/mia-algo-gpu-base.tar.gz

            shift
            ;;
        -l | --uploader)
            if [ -f offline/uploader ]; then
                rm offline/uploader
            fi
            cp uploader/dist/linux/uploader offline/uploader

            shift
            ;;
        -u | --ui)
            if [ -f offline/mia-ui-base.tar.gz ]; then
                rm offline/mia-ui-base.tar.gz
            fi
            docker save mia-ui-base:${UI_BASE_VERSION} | \
                gzip > offline/mia-ui-base.tar.gz

            shift
            ;;
        -d | --dev)
            if [ -f offline/algo-gpu-venv.tar.gz ]; then
                rm offline/algo-gpu-venv.tar.gz
            fi
            tar -C algo-gpu -zcvf offline/algo-gpu-venv.tar.gz .venv/
            if [ -f offline/ui-node-modules.tar.gz ]; then
                rm offline/ui-node-modules.tar.gz
            fi
            tar -C ui -zcvf offline/ui-node-modules.tar.gz node_modules/ \
                package-lock.json package.json
 
            shift
            ;;
        --)
            shift;
            break
            ;;
    esac
done
