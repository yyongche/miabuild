import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { DatabaseModule } from '../src/database/database.module';
import { AcceptTypesModule } from '../src/accept-types/accept-types.module';
import { DatabaseController } from '../test/database.controller';
import { DatabaseService } from '../test/database.service';
import * as request from 'supertest';

describe('AcceptTypes', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DatabaseModule, AcceptTypesModule],
      controllers: [DatabaseController],
      providers: [
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
          }),
        },
        { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
        DatabaseService,
      ],
    }).compile();
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('POST /accept-types (suffix=undefined)', () => {
    return request(app.getHttpServer())
      .post('/accept-types')
      .send({ media_type: 'image' })
      .expect(400);
  });

  it('POST /accept-types (media-type=undefined)', () => {
    return request(app.getHttpServer())
      .post('/accept-types')
      .send({ suffix: 'jpg' })
      .expect(400);
  });

  it('POST /accept-types (suffix=jpg)', async () => {
    return request(app.getHttpServer())
      .post('/accept-types')
      .send({ suffix: 'jpg', media_type: 'image' })
      .expect(201)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            suffix: 'jpg',
            media_type: 'image',
            enabled: true,
          }),
        );
      });
  });

  it('POST /accept-types (suffix=wav)', async () => {
    return request(app.getHttpServer())
      .post('/accept-types')
      .send({ suffix: 'wav', media_type: 'audio' })
      .expect(201)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            suffix: 'wav',
            media_type: 'audio',
            enabled: true,
          }),
        );
      });
  });

  it('POST /accept-types (suffix=mp4)', async () => {
    return request(app.getHttpServer())
      .post('/accept-types')
      .send({ suffix: 'mp4', media_type: 'video' })
      .expect(201)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            suffix: 'mp4',
            media_type: 'video',
            enabled: true,
          }),
        );
      });
  });

  it('GET /accept-types', async () => {
    return request(app.getHttpServer())
      .get('/accept-types')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(3);
      });
  });

  it('GET /accept-types (media-type=image)', async () => {
    return request(app.getHttpServer())
      .get('/accept-types?media-type=image')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(1);
        expect(res.body[0]).toEqual(
          expect.objectContaining({
            suffix: 'jpg',
            media_type: 'image',
            enabled: true,
          }),
        );
      });
  });

  it('GET /accept-types (suffix=jpg)', async () => {
    return request(app.getHttpServer())
      .get('/accept-types/jpg')
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            suffix: 'jpg',
            media_type: 'image',
            enabled: true,
          }),
        );
      });
  });

  it('GET /accept-types (suffix=js)', () => {
    return request(app.getHttpServer()).get('/accept-types/js').expect(404);
  });

  it('PUT /accept-types (suffix==undefined)', () => {
    return request(app.getHttpServer())
      .put('/accept-types')
      .send({ media_type: 'video', enabled: false })
      .expect(400);
  });

  it('PUT /accept-types (suffix=jpg)', async () => {
    return request(app.getHttpServer())
      .put('/accept-types')
      .send({ suffix: 'jpg', media_type: 'video', enabled: false })
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            suffix: 'jpg',
            media_type: 'video',
            enabled: false,
          }),
        );
      });
  });

  it('DELETE /accept-types (suffix=jpg)', () => {
    return request(app.getHttpServer()).delete('/accept-types/jpg').expect(200);
  });

  it('DELETE /accept-types (suffix=wav)', () => {
    return request(app.getHttpServer()).delete('/accept-types/wav').expect(200);
  });

  it('DELETE /accept-types (suffix=mp4)', () => {
    return request(app.getHttpServer()).delete('/accept-types/mp4').expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
