import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { DatabaseModule } from '../src/database/database.module';
import { MessagesModule } from '../src/messages/messages.module';
import { DatabaseController } from '../test/database.controller';
import { DatabaseService } from '../test/database.service';
import * as request from 'supertest';

describe('Messages', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DatabaseModule, MessagesModule],
      controllers: [DatabaseController],
      providers: [
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
          }),
        },
        { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
        DatabaseService,
      ],
    }).compile();
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('DELETE /messages (:before)', () => {
    return request(app.getHttpServer()).delete('/messages').expect(200);
  });

  it('POST /messages (content=undefined)', () => {
    return request(app.getHttpServer()).post('/messages').send({}).expect(400);
  });

  it('POST /messages (content-type=textual)', () => {
    return request(app.getHttpServer())
      .post('/messages')
      .send({ content: 'in percentage' })
      .expect(400);
  });

  it('POST /messages (content-type=json)', () => {
    return request(app.getHttpServer())
      .post('/messages')
      .send({
        content: '{ "message": "in percentage" }',
      })
      .expect(201);
  });

  it('POST /messages (content-type=nested-json)', () => {
    return request(app.getHttpServer())
      .post('/messages')
      .send({
        content: '{ "data": { "score": 10.0, "remark": "in percentage" } }',
      })
      .expect(201);
  });

  it('GET /messages', async () => {
    return request(app.getHttpServer())
      .get('/messages')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(2);
      });
  });

  it('DELETE /messages (:after)', () => {
    return request(app.getHttpServer()).delete('/messages').expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
