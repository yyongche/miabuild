import {
  Controller,
  Delete,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Query,
} from '@nestjs/common';
import { Target } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsIn } from 'class-validator';
import { AcceptTypeDto } from '../src/accept-types/accept-types.transfer';
import { AlgorithmDto } from '../src/algorithms/algorithms.transfer';
import { JobDto } from '../src/jobs/jobs.transfer';
import { DatabaseService } from '../test/database.service';
import * as TargetTransform from '../src/helpers/target.pipe';

class DeleteAlgorithmQueryDto {
  @Transform(TargetTransform.toClass, { toClassOnly: true })
  @Transform(TargetTransform.toPlain, { toPlainOnly: true })
  @IsIn(TargetTransform.getOptions())
  target: Target = Target.DEVELOPMENT;
}

@Controller()
export class DatabaseController {
  constructor(private readonly databaseService: DatabaseService) {}

  @Delete('accept-types/:suffix')
  async deleteAcceptType(
    @Param('suffix') suffix: string,
  ): Promise<AcceptTypeDto> {
    const acceptType = await this.databaseService.deleteAcceptType({
      suffix,
    });
    if (acceptType === null) {
      throw new NotFoundException();
    }
    return new AcceptTypeDto(acceptType);
  }

  @Delete('algorithms/:alias')
  async deleteAlgorithm(
    @Param('alias') alias: string,
    @Query() params: DeleteAlgorithmQueryDto,
  ): Promise<AlgorithmDto> {
    const { target } = params;
    const algorithm = await this.databaseService.deleteAlgorithm({
      alias_target: { alias: alias, target: target },
    });
    if (algorithm === null) {
      throw new NotFoundException();
    }
    return new AlgorithmDto(algorithm);
  }

  @Delete('jobs/:id')
  async deleteJob(@Param('id', ParseUUIDPipe) id: string) {
    const job = await this.databaseService.deleteJob({ id });
    if (job === null) {
      throw new NotFoundException();
    }
    return new JobDto(job);
  }

  @Delete('media-files')
  async deleteMediaFiles() {
    return this.databaseService.deleteMediaFiles();
  }

  @Delete('messages')
  async deleteMessages() {
    return this.databaseService.deleteMessages();
  }
}
