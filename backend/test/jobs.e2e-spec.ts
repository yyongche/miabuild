import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { Test } from '@nestjs/testing';
import { join } from 'path';
import { ApiModule } from '../src/api/api.module';
import { DatabaseModule } from '../src/database/database.module';
import { JobsModule } from '../src/jobs/jobs.module';
import { ResultsModule } from '../src/results/results.module';
import { DatabaseController } from '../test/database.controller';
import { DatabaseService } from '../test/database.service';
import * as request from 'supertest';

describe('Jobs', () => {
  let app: INestApplication;
  const data = { url: '', fid: '', jids: [] };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        EventEmitterModule.forRoot(),
        ApiModule,
        DatabaseModule,
        JobsModule,
        ResultsModule,
      ],
      controllers: [DatabaseController],
      providers: [
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({
            transform: true,
            whitelist: true,
            skipMissingProperties: true,
          }),
        },
        { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
        DatabaseService,
      ],
    }).compile();
    app = moduleRef.createNestApplication();
    await app.init();
  });

  it('POST /algorithms (name=sleep_5s@cpu)', () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({
        alias: 'sleep_5s',
        target: 'cpu',
        name: 'sleep_5s@cpu',
        media_types: ['audio'],
      })
      .expect(201);
  });

  it('POST /algorithms (name=sleep_5s@gpu)', () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({
        alias: 'sleep_5s',
        target: 'gpu',
        name: 'sleep_5s@gpu',
        media_types: ['audio'],
      })
      .expect(201);
  });

  it('POST /algorithms (name=sleep_5s@batch)', () => {
    return request(app.getHttpServer())
      .post('/algorithms')
      .send({
        alias: 'sleep_5s',
        target: 'batch',
        name: 'sleep_5s@batch',
        media_types: ['audio'],
      })
      .expect(201);
  });

  it('POST /media-files (media-type=video)', async () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .field('media_type', 'video')
      .attach('media_file', join(process.cwd(), 'test', 'video.mp4'))
      .expect(201)
      .then((res) => {
        expect(res.body).toHaveProperty('url');
        data.url = res.body.url;
      });
  });

  it('POST /media-files (media-type=audio)', async () => {
    return request(app.getHttpServer())
      .post('/media-files')
      .field('media_type', 'audio')
      .attach('media_file', join(process.cwd(), 'test', 'audio.wav'))
      .expect(201)
      .then((res) => {
        expect(res.body).toHaveProperty('id');
        data.fid = res.body.id;
      });
  });

  it('POST /jobs (:1)', async () => {
    return request(app.getHttpServer())
      .post('/jobs')
      .send({ file_id: `${data.fid}`, labels: ['images'] })
      .expect(201)
      .then((res) => {
        expect(res.body.interactive).toEqual(true);
        expect(res.body).toHaveProperty('id');
        data.jids.push(res.body.id);
      });
  });

  it('POST /jobs (:2)', async () => {
    return request(app.getHttpServer())
      .post('/jobs')
      .send({ file_id: `${data.fid}` })
      .expect(201)
      .then((res) => {
        expect(res.body.interactive).toEqual(true);
        expect(res.body).toHaveProperty('id');
        data.jids.push(res.body.id);
      });
  });

  it('POST /jobs/media-files (:3)', async () => {
    return request(app.getHttpServer())
      .post('/jobs/media-files')
      .field('media_type', 'audio')
      .field('path', '/assets/audio.wav')
      .attach('media_file', join(process.cwd(), 'test', 'audio.wav'))
      .expect(201)
      .then((res) => {
        expect(res.body.interactive).toEqual(false);
        expect(res.body).toHaveProperty('id');
        data.jids.push(res.body.id);
      });
  });

  it('POST /jobs (:4)', async () => {
    return request(app.getHttpServer())
      .post('/jobs')
      .send({
        file_id: `${data.fid}`,
        interactive: false,
        algorithms: [
          { alias: 'sleep_5s', target: 'cpu' },
          { alias: 'sleep_5s', target: 'gpu' },
        ],
      })
      .expect(201)
      .then((res) => {
        expect(res.body.interactive).toEqual(false);
        expect(res.body).toHaveProperty('id');
        data.jids.push(res.body.id);
      });
  });

  it('POST /jobs/media-files (:5)', async () => {
    return request(app.getHttpServer())
      .post('/jobs/media-files')
      .field('media_type', 'audio')
      .field('path', '/assets/audio.wav')
      .field('mode', 'interactive')
      .attach('media_file', join(process.cwd(), 'test', 'audio.wav'))
      .expect(201)
      .then((res) => {
        expect(res.body.interactive).toEqual(true);
        expect(res.body).toHaveProperty('id');
        data.jids.push(res.body.id);
      });
  });

  it('GET /jobs', async () => {
    return request(app.getHttpServer())
      .get('/jobs')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(5);
      });
  });

  it('GET /jobs (label=images)', async () => {
    return request(app.getHttpServer())
      .get('/jobs?label=images')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(1);
      });
  });

  it('GET /jobs (mode=interactive)', async () => {
    return request(app.getHttpServer())
      .get('/jobs?mode=interactive')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(3);
      });
  });

  it('GET /jobs (mode=batch)', async () => {
    return request(app.getHttpServer())
      .get('/jobs?mode=batch')
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(2);
      });
  });

  it('GET /jobs/media_files', async () => {
    return request(app.getHttpServer())
      .get(`/jobs/media-files/${data.fid}`)
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(3);
      });
  });

  it('GET /jobs (:2)', async () => {
    return request(app.getHttpServer())
      .get(`/jobs/${data.jids[1]}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty('file');
        expect(res.body).toHaveProperty('algorithms');
        expect(res.body).toHaveProperty('results');
        expect(res.body.algorithms.length).toEqual(2);
        expect(res.body.results.length).toEqual(0);
        expect(res.body).toEqual(
          expect.objectContaining({
            id: `${data.jids[1]}`,
            file_id: `${data.fid}`,
            interactive: true,
          }),
        );
      });
  });

  it('GET /jobs (:3)', async () => {
    return request(app.getHttpServer())
      .get(`/jobs/${data.jids[2]}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty('file');
        expect(res.body).toHaveProperty('algorithms');
        expect(res.body).toHaveProperty('results');
        expect(res.body.algorithms.length).toEqual(1);
        expect(res.body.results.length).toEqual(0);
        expect(res.body).toEqual(
          expect.objectContaining({
            id: `${data.jids[2]}`,
            interactive: false,
          }),
        );
      });
  });

  it('GET /jobs (:4)', async () => {
    return request(app.getHttpServer())
      .get(`/jobs/${data.jids[3]}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty('file');
        expect(res.body).toHaveProperty('algorithms');
        expect(res.body).toHaveProperty('results');
        expect(res.body.algorithms.length).toEqual(2);
        expect(res.body.results.length).toEqual(0);
        expect(res.body).toEqual(
          expect.objectContaining({
            id: `${data.jids[3]}`,
            file_id: `${data.fid}`,
            interactive: false,
          }),
        );
      });
  });

  it('POST /results (result-type=json)', async () => {
    return request(app.getHttpServer())
      .post('/results')
      .send({
        job_id: `${data.jids[2]}`,
        alias: 'sleep_5s',
        target: 'batch',
        result_type: 'json',
        content: '{ "score": 10.0, "remark": "in percentage" }',
      })
      .expect(201)
      .then((res) => {
        expect(res.body).toHaveProperty('url');
        expect(res.body.url).toBeNull();
      });
  });

  it('POST /results (result-type=video)', async () => {
    return request(app.getHttpServer())
      .post(`/results?url=${data.url}`)
      .send({
        job_id: `${data.jids[2]}`,
        alias: 'sleep_5s',
        target: 'batch',
        result_type: 'video',
        content: '{ "score": 10.0, "remark": "in percentage" }',
      })
      .expect(201)
      .then((res) => {
        expect(res.body).toHaveProperty('url');
        expect(res.body.url).toEqual(`${data.url}`);
      });
  });

  it('POST /results (result-type=image)', () => {
    return request(app.getHttpServer())
      .post('/results')
      .field('job_id', `${data.jids[2]}`)
      .field('alias', 'sleep_5s')
      .field('target', 'batch')
      .field('result_type', 'image')
      .field('content', '{ "score": 10.0, "remark": "in percentage" }')
      .attach('result_file', join(process.cwd(), 'test', 'image.png'))
      .expect(201);
  });

  it('POST /results (result-file=undefined)', () => {
    return request(app.getHttpServer())
      .post('/results')
      .send({
        job_id: `${data.jids[2]}`,
        alias: 'sleep_5s',
        target: 'batch',
        result_type: 'audio',
        content: '{ "score": 10.0, "remark": "in percentage" }',
      })
      .expect(400);
  });

  it('GET /results/jobs', async () => {
    return request(app.getHttpServer())
      .get(`/results/jobs/${data.jids[2]}`)
      .expect(200)
      .then((res) => {
        expect(res.body.length).toEqual(3);
      });
  });

  it('UPDATE /job (:3)', () => {
    return request(app.getHttpServer())
      .put('/jobs')
      .send({ id: `${data.jids[2]}`, status: 'completed' })
      .expect(200);
  });

  it('GET /jobs (:3,status=completed)', async () => {
    return request(app.getHttpServer())
      .get(`/jobs/${data.jids[2]}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty('file');
        expect(res.body).toHaveProperty('algorithms');
        expect(res.body).toHaveProperty('results');
        expect(res.body.algorithms.length).toEqual(1);
        expect(res.body.results.length).toEqual(3);
        expect(res.body).toEqual(
          expect.objectContaining({
            id: `${data.jids[2]}`,
            interactive: false,
            status: 'completed',
          }),
        );
      });
  });

  it('DELETE /jobs (:1)', () => {
    return request(app.getHttpServer())
      .delete(`/jobs/${data.jids[0]}`)
      .expect(200);
  });

  it('DELETE /jobs (:2)', () => {
    return request(app.getHttpServer())
      .delete(`/jobs/${data.jids[1]}`)
      .expect(200);
  });

  it('DELETE /jobs (:3)', () => {
    return request(app.getHttpServer())
      .delete(`/jobs/${data.jids[2]}`)
      .expect(200);
  });

  it('DELETE /jobs (:4)', () => {
    return request(app.getHttpServer())
      .delete(`/jobs/${data.jids[3]}`)
      .expect(200);
  });

  it('DELETE /jobs (:5)', () => {
    return request(app.getHttpServer())
      .delete(`/jobs/${data.jids[4]}`)
      .expect(200);
  });

  it('DELETE /media-files', () => {
    return request(app.getHttpServer()).delete('/media-files').expect(200);
  });

  it('DELETE /algorithms (name=sleep_5s@cpu)', () => {
    return request(app.getHttpServer())
      .delete('/algorithms/sleep_5s?target=cpu')
      .expect(200);
  });

  it('DELETE /algorithms (name=sleep_5s@cpu)', () => {
    return request(app.getHttpServer())
      .delete('/algorithms/sleep_5s?target=gpu')
      .expect(200);
  });

  it('DELETE /algorithms (name=sleep_5s)', () => {
    return request(app.getHttpServer())
      .delete('/algorithms/sleep_5s?target=batch')
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
