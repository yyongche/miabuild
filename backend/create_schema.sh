#!/usr/bin/env bash

npx prisma db push --force-reset

source .env
n=${#DATABASE_URL}-14
pg_dump --dbname=${DATABASE_URL:0:n} > ../mia-schema.sql
