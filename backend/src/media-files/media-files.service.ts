import { Injectable } from '@nestjs/common';
import { MediaFile, Prisma } from '@prisma/client';
import { PrismaService } from '../database/prisma.service';

@Injectable()
export class MediaFilesService {
  constructor(private prisma: PrismaService) {}

  async mediaFile(where: Prisma.MediaFileWhereUniqueInput): Promise<MediaFile> {
    return this.prisma.mediaFile.findUnique({ where });
  }

  async mediaFiles(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.MediaFileWhereUniqueInput;
    where?: Prisma.MediaFileWhereInput;
    orderBy?: Prisma.MediaFileOrderByWithRelationInput;
  }): Promise<MediaFile[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.mediaFile.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createMediaFile(data: Prisma.MediaFileCreateInput): Promise<MediaFile> {
    return this.prisma.mediaFile.create({ data });
  }

  async updateMediaFile(params: {
    where: Prisma.MediaFileWhereUniqueInput;
    data: Prisma.MediaFileUpdateInput;
  }): Promise<MediaFile> {
    const { where, data } = params;
    return this.prisma.mediaFile.update({ data, where });
  }
}
