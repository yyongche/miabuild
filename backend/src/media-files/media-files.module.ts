import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { DatastoreModule } from '../datastore/datastore.module';
import { MessagesModule } from '../messages/messages.module';
import { MediaFilesController } from './media-files.controller';
import { MediaFilesService } from './media-files.service';

@Module({
  imports: [DatabaseModule, MessagesModule, DatastoreModule],
  controllers: [MediaFilesController],
  providers: [MediaFilesService],
  exports: [MediaFilesService],
})
export class MediaFilesModule {}
