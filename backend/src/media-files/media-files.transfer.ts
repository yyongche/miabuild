import { MediaFile, MediaType } from '@prisma/client';
import { Expose, Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsDefined,
  IsIn,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import * as MediaTypeTransform from '../helpers/media-type.pipe';

export class ListMediaFilesQueryDto {
  @IsString()
  path: string;

  @IsString()
  label: string;

  @Expose({ name: 'media-type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsIn(MediaTypeTransform.getOptions())
  mediaType: MediaType;

  @Expose({ name: 'start-date' })
  @Type(() => Date)
  @IsDate()
  startDate: Date;

  @Expose({ name: 'end-date' })
  @Type(() => Date)
  @IsDate()
  endDate: Date;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class CreateMediaFileFormDto {
  @Expose({ name: 'media_type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(MediaTypeTransform.getOptions())
  type: MediaType;

  @IsString()
  path: string;
}

export class UpdateMediaFileDto {
  @IsDefined()
  @IsUUID()
  id: string;

  @IsArray()
  @IsString({ each: true })
  labels: string[];
}

export class MediaFileDto {
  id: string;

  @Expose({ name: 'media_type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  type: MediaType;

  @Expose({ toPlainOnly: true })
  url: string;

  path: string;

  labels: string[];

  @Expose({ toPlainOnly: true })
  created: Date;

  @Expose({ toPlainOnly: true })
  modified: Date;

  constructor(partial: Partial<MediaFile>) {
    Object.assign(this, partial);
  }
}
