import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { PrismaService } from './database/prisma.service';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const prisma = app.get(PrismaService);
  await prisma.enableShutdownHooks(app);

  const config = new DocumentBuilder()
    .setTitle('MIA Backend')
    .setDescription('MIA Backend API')
    .setVersion('3.1')
    .build();
  const doc = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('openapi', app, doc);

  await app.listen(3000);
}
bootstrap();
