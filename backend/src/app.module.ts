import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ScheduleModule } from '@nestjs/schedule';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ApiModule } from './api/api.module';
import { DatabaseModule } from './database/database.module';
import { DatastoreModule } from './datastore/datastore.module';
import { AcceptTypesModule } from './accept-types/accept-types.module';
import { AlgorithmsModule } from './algorithms/algorithms.module';
import { JobsModule } from './jobs/jobs.module';
import { MediaFilesModule } from './media-files/media-files.module';
import { MessagesModule } from './messages/messages.module';
import { ResultsModule } from './results/results.module';
import { InitService } from './init.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    EventEmitterModule.forRoot(),
    ScheduleModule.forRoot(),
    ServeStaticModule.forRoot({
      rootPath: join(process.cwd(), 'static'),
      serveRoot: '/static',
    }),
    ApiModule,
    DatabaseModule,
    DatastoreModule,
    AcceptTypesModule,
    AlgorithmsModule,
    JobsModule,
    MediaFilesModule,
    MessagesModule,
    ResultsModule,
  ],
  providers: [
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        transform: true,
        whitelist: true,
        skipMissingProperties: true,
      }),
    },
    { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
    InitService,
  ],
})
export class AppModule {}
