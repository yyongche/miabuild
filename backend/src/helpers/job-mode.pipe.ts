export function getOptions() {
  return ['BATCH', 'INTERACTIVE'];
}

export function toClass({ value }) {
  return value?.toUpperCase();
}

export function toPlain({ value }) {
  return value?.toLowerCase();
}

export function isInteractive(mode: string): boolean {
  return mode === 'INTERACTIVE';
}
