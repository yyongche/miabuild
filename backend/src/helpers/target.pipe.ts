import { Target } from '@prisma/client';

export function getOptions() {
  return Object.values(Target);
}

function plainToClass(value: any) {
  if (typeof value === 'string') {
    switch (value) {
      case 'dev':
        value = 'development';
        break;
      case 'cpu':
        value = 'cpu_machine';
        break;
      case 'gpu':
        value = 'gpu_machine';
        break;
      default:
    }
  }
  return value?.toUpperCase();
}

function classToPlain(value: any) {
  if (typeof value === 'string') {
    switch (value) {
      case 'DEVELOPMENT':
        value = 'DEV';
        break;
      case 'CPU_MACHINE':
        value = 'CPU';
        break;
      case 'GPU_MACHINE':
        value = 'GPU';
        break;
      default:
    }
  }
  return value?.toLowerCase();
}

export function toClass({ value }) {
  return plainToClass(value);
}

export function toPlain({ value }) {
  return classToPlain(value);
}
