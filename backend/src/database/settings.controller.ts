import { Body, Controller, Get, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SettingsService } from './settings.service';
import { SettingsDto, UpdateSettingsDto } from './settings.transfer';

@ApiTags('settings')
@Controller('settings')
export class SettingsController {
  constructor(private readonly settingsService: SettingsService) {}

  @Get()
  async getSettings(): Promise<SettingsDto> {
    const settings = await this.settingsService.settings();
    return new SettingsDto(settings);
  }

  @Put()
  async updateSettings(@Body() data: UpdateSettingsDto): Promise<SettingsDto> {
    const settings = await this.settingsService.updateSettings(data);
    return new SettingsDto(settings);
  }
}
