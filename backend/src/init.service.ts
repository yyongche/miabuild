import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { SettingsService } from './database/settings.service';

@Injectable()
export class InitService implements OnApplicationBootstrap {
  constructor(private readonly settingsService: SettingsService) {}

  async onApplicationBootstrap() {
    const settings = await this.settingsService.settings();
    if (!settings.initialized) {
      await this.settingsService.initialize();
    }
  }
}
