import { AcceptType, MediaType } from '@prisma/client';
import { Expose, Transform, Type } from 'class-transformer';
import {
  IsBoolean,
  IsDefined,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import * as MediaTypeTransform from '../helpers/media-type.pipe';

export class ListAcceptTypesQueryDto {
  @Expose({ name: 'media-type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsIn(MediaTypeTransform.getOptions())
  mediaType: MediaType;

  @Type(() => Number)
  @IsNumber()
  offset = 0;

  @Type(() => Number)
  @IsNumber()
  limit = 100;
}

export class CreateAcceptTypeDto {
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  suffix: string;

  @Expose({ name: 'media_type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsDefined()
  @IsIn(MediaTypeTransform.getOptions())
  type: MediaType;

  @IsBoolean()
  enabled: boolean;
}

export class UpdateAcceptTypeDto {
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  suffix: string;

  @Expose({ name: 'media_type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  @IsIn(MediaTypeTransform.getOptions())
  type: MediaType;

  @IsBoolean()
  enabled: boolean;
}

export class AcceptTypeDto {
  suffix: string;

  @Expose({ name: 'media_type' })
  @Transform(MediaTypeTransform.toClass, { toClassOnly: true })
  @Transform(MediaTypeTransform.toPlain, { toPlainOnly: true })
  type: MediaType;

  enabled: boolean;

  @Expose({ toPlainOnly: true })
  created: Date;

  @Expose({ toPlainOnly: true })
  modified: Date;

  constructor(partial: Partial<AcceptType>) {
    Object.assign(this, partial);
  }
}
