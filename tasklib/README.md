# API for Job Task

## API Object

### Creation

API object is created as shown below.

```python
@app.task(name="algorithm.xxx")
def run_xxx(t, _):
    api = API(t) # API object
```

### Properties

API object has the following properties:

- `assetdir`: asset directory;
- `outdir`: output directory; and
- `infile`: input media file.

It also has the following methods.

### Download File

`download_file` will download a file from the datastore.

### Publish Message

`publish_message` will publish a message to the backend.

The message is in the form of either a string or a dictionary.

### Publish Result File

`publish_result` will publish result file to the backend.

The result is in the form of:

- a media file in the output directory;
- a string (`image` | `audio` | `video`); and
- an optional description.

### Publish Result Dictionary

`publish_result_dict` will publish result dictionary to the backend.

The result is in the form of:

- a dictionary; and
- an optional description.
