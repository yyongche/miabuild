#!/usr/bin/env python3
"""
Application for Job Task.
"""

from datetime import datetime
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union
import hashlib
import mimetypes
import os

from celery import Celery, chord, group, shared_task, signature
from celery.app.control import Control
from kombu.serialization import register
from minio import Minio
import requests

from .json import json, dumps, loads
from .task import Task
from .settings import BACKEND_URL, ASSET_FOLDER, WIP_FOLDER, REDIS_URL
from .settings import (MINIO_ENDPOINT, MINIO_PORT, MINIO_ACCESS_KEY,
                       MINIO_SECRET_KEY)

register("miajson", dumps, loads, "application/json")


class CeleryConfig:  # pylint: disable=too-few-public-methods
    """Celery configuration."""
    task_serializer = "miajson"
    accept_content = ["application/json"]


Message = Union[Dict[str, Any], str]


class CeleryApp:
    """Celery application."""

    def __init__(self, *, celery: Optional[Celery] = None):
        self._backend_url = BACKEND_URL
        self._asset_dir = ASSET_FOLDER
        self._wip_dir = WIP_FOLDER

        if celery is None:
            self._celery = Celery(broker=REDIS_URL, backend=REDIS_URL)
            self._celery.config_from_object(CeleryConfig)
        else:
            self._celery = celery

        self._minio = Minio(f"{MINIO_ENDPOINT}:{MINIO_PORT}",
                            secure=False,
                            access_key=MINIO_ACCESS_KEY,
                            secret_key=MINIO_SECRET_KEY)

    @property
    def backend_url(self) -> str:
        """Return URL of the backend."""
        return self._backend_url

    @property
    def asset_dir(self) -> Path:
        """Return the asset directory."""
        return self._asset_dir

    @property
    def wip_dir(self) -> Path:
        """Return the working directory."""
        return self._wip_dir

    @property
    def celery(self) -> Celery:
        """Return the Celery object."""
        return self._celery

    @property
    def minio(self) -> Minio:
        """Return the Minio object."""
        return self._minio

    def publish_message(self, message: Message, source: Optional[str] = None):
        """Publish a message to the backend."""
        content = message
        if not isinstance(message, dict):
            content = {"message": str(message)}
        payload = {"content": json.dumps(content)}
        if isinstance(source, str):
            payload["source"] = source
        requests.post(f"{self._backend_url}/messages",
                      json=payload,
                      timeout=10)

    @classmethod
    def get_datastore_id(cls, file_url: str) -> Optional[Tuple[str, str]]:
        """
        Retrieve its file identifier (bucket and object names)
        when a file is in the datastore.
        """
        if file_url.startswith("/store/") and len(
                file_url[1:].split("/")) >= 3:
            _, bucket_name, object_name = file_url[1:].split("/")[:3]
            return (bucket_name, object_name)
        return None

    def download_file(self,
                      object_name: str,
                      dest_dir: Path,
                      *,
                      bucket_name: str = "media-files") -> Path:
        """Download a file from the datastore to a directory."""
        object_data = None
        try:
            object_data = self._minio.get_object(bucket_name, object_name)
            with open(dest_dir / object_name, "wb") as file_data:
                for data in object_data.stream(32 * 1024):
                    file_data.write(data)
        finally:
            if object_data is not None:
                object_data.close()
                object_data.release_conn()
        return dest_dir / object_name

    def upload_file(self,
                    src_file: Path,
                    *,
                    bucket_name: str = "result-files") -> str:
        """"Upload a file to the datastore."""
        object_name = hashlib.md5(
            datetime.now().isoformat().encode()).hexdigest() + src_file.suffix
        with open(src_file, "rb") as file_data:
            file_stat = os.stat(src_file)
            file_type, _ = mimetypes.guess_type(src_file)
            if file_type is None:
                self._minio.put_object(bucket_name, object_name, file_data,
                                       file_stat.st_size)
            else:
                self._minio.put_object(bucket_name, object_name, file_data,
                                       file_stat.st_size, file_type)
        return f"/store/{bucket_name}/{object_name}"


@shared_task(name="mia.app.mark_job_as_completed")
def mark_job_as_completed(*_, **kwargs):
    """Mark a job in the backend as completed."""
    payload = {"id": kwargs["jid"], "status": "completed"}
    requests.put(f"{BACKEND_URL}/jobs", json=payload, timeout=10)


class TaskControl:
    """Task control."""

    @classmethod
    def tasks(cls, app: CeleryApp) -> Dict[str, List[str]]:
        """Return the tasks."""
        tasks = {}
        control = Control(app.celery)
        active: Optional[Dict[str, Any]] = control.inspect().active()
        if active is not None:
            for _, tks in active.items():
                tasks["active"] = (str(t["id"]) for t in tks)
        reserved: Optional[Dict[str, Any]] = control.inspect().reserved()
        if reserved is not None:
            for _, tks in reserved.items():
                tasks["reserved"] = (str(t["id"]) for t in tks)
        scheduled: Optional[Dict[str, Any]] = control.inspect().scheduled()
        if scheduled is not None:
            for _, tks in scheduled.items():
                tasks["scheduled"] = (str(t["request"]["id"]) for t in tks)
        return tasks

    @classmethod
    def invoke_job(cls, app: CeleryApp, job: Dict[str, Any]) -> List[Task]:
        """Invoke tasks derived from a job in the backend."""
        jid, size = job["id"], len(job["algorithms"])
        tasks = [Task.create(job, i) for i in range(size)]
        chord(
            group(
                signature(f"algorithm.{task.alias}",
                          args=(task, task.params),
                          task_id=f"{task.jid}.{i}",
                          app=app.celery,
                          queue=task.target) for i, task in enumerate(tasks)
                if task is not None),
            signature("mia.app.mark_job_as_completed",
                      kwargs={"jid": jid},
                      app=app.celery)).apply_async()
        return [task for task in tasks if task is not None]

    @classmethod
    def terminate_job(cls, app: CeleryApp, jid: str) -> List[str]:
        """Terminate tasks associated with a job."""
        tasks = []
        control = Control(app.celery)
        active: Optional[Dict[str, Any]] = control.inspect().active()
        if active is not None:
            for _, tks in active.items():
                tasks.extend([t for t in tks if t["id"].startswith(jid)])
        reserved: Optional[Dict[str, Any]] = control.inspect().reserved()
        if reserved is not None:
            for _, tks in reserved.items():
                tasks.extend([t for t in tks if t["id"].startswith(jid)])
        scheduled: Optional[Dict[str, Any]] = control.inspect().scheduled()
        if scheduled is not None:
            for _, tks in scheduled.items():
                tasks.extend([
                    t["request"] for t in tks
                    if t["request"]["id"].startswith(jid)
                ])
        control.terminate([t["id"] for t in tasks])
        return [str(t["id"]) for t in tasks]
