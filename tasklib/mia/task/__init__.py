#!/usr/bin/env python3
"""
API for Job Task.
"""

from .api import JobTask, mia_app

app = mia_app.celery
API = JobTask
