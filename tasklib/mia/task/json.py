#!/usr/bin/env python3
"""
JSON encoder and decoder for Job Task.
"""

from typing import Any, Dict

from superjson import SuperJson, get_class_name

from .task import Task

TASK_CLASS_NAME = get_class_name(Task("", "", {}, "", ""))


class MiaJson(SuperJson):
    """JSON encoder and decoder."""

    def dump_Task(  # pylint: disable=invalid-name
            self,
            obj: Task,
            class_name: str = TASK_CLASS_NAME) -> Dict[str, Any]:
        """Return the Task object in JSON representation."""
        return {
            "$" + class_name: {
                "_jid": obj.jid,
                "_file": obj.file,
                "_params": obj.params,
                "_alias": obj.alias,
                "_target": obj.target
            }
        }

    def load_Task(  # pylint: disable=invalid-name
            self,
            dct: Dict[str, Any],
            class_name: str = TASK_CLASS_NAME) -> Task:
        """Return the Task object from JSON representation."""
        return Task(**dct["$" + class_name])


json = MiaJson()


def dumps(obj):
    """Return the object in JSON representation."""
    return json.dumps(obj)


def loads(s: str):  # pylint: disable=invalid-name
    """Return the object from JSON representation."""
    return json.loads(s)
