#!/usr/bin/env bash

if [ -d .venv ]; then
    pipenv --rm
    rm -rf .venv/
fi
export PIPENV_VENV_IN_PROJECT=1
pipenv install --dev --python 3.8
